-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2018 at 03:30 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `system812`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `customer_fname` varchar(50) DEFAULT NULL,
  `customer_mname` varchar(50) DEFAULT NULL,
  `customer_lname` varchar(50) DEFAULT NULL,
  `customer_address` varchar(50) DEFAULT NULL,
  `customer_contact` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_fname`, `customer_mname`, `customer_lname`, `customer_address`, `customer_contact`) VALUES
(1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `quantity` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `price`, `category`, `description`, `quantity`) VALUES
(1600453, 'LuckyMe Jjamppong Noodles', 16.25, 'Noodles', '60 nWT.', 47),
(1600454, 'LuckyMe Chicken Noodles', 56.75, 'Noodles', '60 nWT.', 56),
(1600455, 'LuckyMe Beef Noodles', 45.5, 'Noodles', '60 nWT.', 21),
(1600456, 'Yacult Probiotic Drink', 60, 'Drink', '12 pcs.', 84),
(1600457, 'RedBull Energy Drink', 34.25, 'Drink', '300ml.', 40),
(1600458, 'Mogu Mogu', 34.5, 'Drink', '300 ml.', 24),
(1600459, 'HBW Black Ballpen', 14.55, 'Ballpen', 'Black', 45),
(1600460, 'HBW Red Ballpen', 14.55, 'Ballpen', 'Red', 48),
(1600461, 'HBW Blue Ballpen', 14.55, 'Ballpen', 'Blue', 50),
(1600462, 'Pillows', 11.25, 'Crackers', '50g nWT.', 0),
(1600463, 'Patata Cracker', 24.5, 'Crackers', '100g nWT.', 30),
(1600464, 'Korean Soju', 115.5, 'Alcohol', '500 ml.', 50),
(1600465, 'Japanese Ramen Noodles', 45.5, 'Noodles', '60 nWT.', 30);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `purchase_id` int(11) NOT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `purchase_quantity` int(11) DEFAULT NULL,
  `purchase_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `receipt_id`, `product_id`, `purchase_quantity`, `purchase_price`) VALUES
(104, 1, 1600454, 1, 56.75),
(105, 1, 1600455, 1, 45.5),
(106, 1, 1600456, 1, 60),
(107, 1, 1600457, 1, 34.25),
(108, 2, 1600458, 1, 34.5),
(109, 2, 1600459, 2, 14.55),
(110, 2, 1600456, 1, 60),
(111, 3, 1600454, 1, 56.75),
(112, 4, 1600453, 1, 16.25),
(113, 5, 1600453, 1, 16.25),
(114, 6, 1600454, 1, 56.75),
(115, 6, 1600455, 1, 45.5),
(116, 6, 1600456, 1, 60),
(117, 7, 1600455, 1, 45.5),
(118, 8, 1600454, 1, 56.75),
(119, 9, 1600454, 1, 56.75),
(120, 9, 1600455, 1, 45.5),
(121, 10, 1600454, 1, 56.75),
(122, 10, 1600455, 1, 45.5),
(123, 11, 1600454, 1, 56.75),
(124, 11, 1600455, 1, 45.5),
(125, 12, 1600453, 1, 16.25),
(126, 13, 1600456, 1, 60),
(127, 13, 1600458, 1, 34.5),
(128, 14, 1600454, 1, 56.75),
(129, 15, 1600465, 10, 45.5),
(130, 16, 1600454, 1, 56.75),
(131, 17, 1600454, 1, 56.75),
(132, 18, 1600456, 1, 60),
(138, 19, 1600457, 1, 34.25),
(139, 19, 1600458, 1, 34.5),
(140, 20, 1600454, 1, 56.75),
(141, 21, 1600454, 1, 56.75),
(142, 21, 1600455, 1, 45.5),
(143, 22, 1600455, 1, 45.5),
(144, 22, 1600456, 10, 60),
(145, 22, 1600457, 1, 34.25),
(146, 23, 1600462, 60, 11.25);

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `receipt_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `order_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`receipt_id`, `user_id`, `customer_id`, `total_qty`, `total_amount`, `order_date`) VALUES
(1, 1, 1, 4, 196.5, '2018-10-02 20:54:57'),
(2, 1, 1, 4, 123.6, '2018-10-02 20:57:14'),
(3, 1, 1, 1, 56.75, '2018-10-02 21:02:06'),
(4, 1, 1, 1, 16.25, '2018-10-02 21:03:20'),
(5, 1, 1, 1, 16.25, '2018-10-02 21:05:01'),
(6, 3, 1, 3, 162.25, '2018-10-02 22:08:55'),
(7, 1, 1, 1, 45.5, '2018-10-03 15:21:42'),
(8, 1, 1, 1, 56.75, '2018-10-03 15:25:13'),
(9, 1, 1, 2, 102.25, '2018-10-03 15:27:57'),
(10, 1, 1, 2, 102.25, '2018-10-03 15:32:51'),
(11, 1, 1, 2, 102.25, '2018-10-03 16:41:44'),
(12, 1, 1, 1, 16.25, '2018-10-04 08:44:54'),
(13, 1, 1, 2, 94.5, '2018-10-04 08:47:32'),
(14, 1, 1, 1, 56.75, '2018-10-04 09:00:54'),
(15, 1, 1, 10, 455, '2018-10-05 20:00:39'),
(16, 1, 1, 1, 56.75, '2018-10-05 01:42:41'),
(17, 1, 1, 1, 56.75, '2018-10-05 07:38:22'),
(18, 1, 1, 1, 60, '2018-10-05 07:38:54'),
(19, 1, 1, 2, 68.75, '2018-10-06 05:26:15'),
(20, 1, 1, 1, 56.75, '2018-10-06 06:15:58'),
(21, 1, 1, 2, 102.25, '2018-10-09 09:46:11'),
(22, 1, 1, 12, 679.75, '2018-10-09 11:37:52'),
(23, 1, 1, 60, 675, '2018-10-09 11:40:35');

-- --------------------------------------------------------

--
-- Stand-in structure for view `salesfinal`
-- (See below for the actual view)
--
CREATE TABLE `salesfinal` (
`purchase_id` int(11)
,`receipt_id` int(11)
,`user_fname` varchar(50)
,`product_name` varchar(50)
,`purchase_quantity` int(11)
,`purchase_price` double
,`total_amount` double
,`order_date` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `stock_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `supply_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `purchase_price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `product_id`, `supply_id`, `quantity`, `purchase_price`) VALUES
(2, 1600454, 1, 1, 56.75),
(3, 1600455, 1, 1, 45.4),
(4, 1600454, 2, 2, 113.5),
(5, 1600454, 3, 1, 56.75),
(6, 1600460, 3, 1, 14.55),
(7, 1600454, 4, 48, 56.75),
(8, 1600456, 5, 50, 60),
(9, 1600454, 6, 96, 56.75),
(10, 1600455, 7, 18, 45.5),
(11, 1600454, 8, 10, 56.75),
(12, 1600454, 9, 10, 56.75),
(13, 1600462, 10, 10, 11.25);

-- --------------------------------------------------------

--
-- Stand-in structure for view `stockreport`
-- (See below for the actual view)
--
CREATE TABLE `stockreport` (
`stock_id` int(11)
,`supply_id` int(11)
,`supplier_id` int(11)
,`supplier_name` varchar(50)
,`product_name` varchar(50)
,`quantity` int(11)
,`purchase_price` double
,`date_receive` date
);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(50) DEFAULT NULL,
  `supplier_address` varchar(50) DEFAULT NULL,
  `supplier_contact` varchar(50) DEFAULT NULL,
  `supplier_email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `supplier_name`, `supplier_address`, `supplier_contact`, `supplier_email`) VALUES
(1, 'Aguilar Dealers', 'Malacanang Palace', '09064271600', 'aguilar.co@gmail.com'),
(2, 'Nestle Company', 'Naga City Cebu', '09054271611', 'nestleco@gmail.com'),
(3, 'Green Cross Corporation', 'Manila, maynila', '09065417066', 'Greencross.gmail.com'),
(4, 'Black Market Dealers', 'Unknown', '0909909099', 'blackmd.hide@yahoo.com'),
(5, 'Daiso Co.', 'Saitma Japan', '09064271600', 'daiso.co@gmail.com'),
(7, 'Asahi Japan CO.', 'Asahi Japan', '09055544211', 'asahijpn@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `supply`
--

CREATE TABLE `supply` (
  `supply_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supply_receipt` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_receive` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supply`
--

INSERT INTO `supply` (`supply_id`, `supplier_id`, `supply_receipt`, `user_id`, `date_receive`) VALUES
(1, 1, 1, 1, '2018-10-01'),
(2, 1, 2, 1, '2018-10-01'),
(3, 2, 3, 1, '2018-10-01'),
(4, 5, 4, 1, '2018-10-02'),
(5, 2, 5, 1, '2018-10-02'),
(6, 1, 6, 1, '2018-10-02'),
(7, 2, 7, 1, '2018-10-02'),
(8, 1, 8, 1, '2018-10-05'),
(9, 5, 9, 1, '2018-10-09'),
(10, 7, 10, 1, '2018-10-09');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `user_fname` varchar(50) DEFAULT NULL,
  `user_mname` varchar(50) DEFAULT NULL,
  `user_lname` varchar(50) DEFAULT NULL,
  `contact_no` bigint(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `login_username` varchar(50) DEFAULT NULL,
  `login_password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_fname`, `user_mname`, `user_lname`, `contact_no`, `birthday`, `address`, `email`, `login_username`, `login_password`) VALUES
(1, 'Admin', 'Santy', 'Sean', 'Bactol', 9064271600, '1999-04-18', 'San Miguel Nabua Camarines Sur', 'santysean72@yahoo.com', 'necrophilla', 'santy123'),
(3, 'Cashier', 'Stevena', 'Aguilar', 'Bactol', 9064271600, '1999-04-18', 'Buenavista III General Trias Cavite', 'steven@yahoo.com', 'steven', '09064271600'),
(4, 'Cashier', 'Nana', 'RT', 'Zoella', 9064271600, '1999-04-18', 'Tokyo, Japan', 'nanazoella@yahoo.com', 'Nana', 'santysean'),
(5, 'Cashier', 'Shan', 'Aguilar', 'Bactol', 9064271600, '1999-04-18', 'Cavite City', 'shanbactol@yahoo.com', 'santysean99', '09064271600');

-- --------------------------------------------------------

--
-- Structure for view `salesfinal`
--
DROP TABLE IF EXISTS `salesfinal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `salesfinal`  AS  select `a`.`purchase_id` AS `purchase_id`,`b`.`receipt_id` AS `receipt_id`,`d`.`user_fname` AS `user_fname`,`c`.`product_name` AS `product_name`,`a`.`purchase_quantity` AS `purchase_quantity`,`a`.`purchase_price` AS `purchase_price`,(`a`.`purchase_quantity` * `a`.`purchase_price`) AS `total_amount`,`b`.`order_date` AS `order_date` from (((`purchase` `a` join `receipt` `b`) join `product` `c`) join `user` `d` on(((`b`.`receipt_id` = `a`.`receipt_id`) and (`a`.`product_id` = `c`.`product_id`) and (`b`.`user_id` = `d`.`user_id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `stockreport`
--
DROP TABLE IF EXISTS `stockreport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stockreport`  AS  select `a`.`stock_id` AS `stock_id`,`b`.`supply_id` AS `supply_id`,`c`.`supplier_id` AS `supplier_id`,`c`.`supplier_name` AS `supplier_name`,`d`.`product_name` AS `product_name`,`a`.`quantity` AS `quantity`,`a`.`purchase_price` AS `purchase_price`,`b`.`date_receive` AS `date_receive` from (((`stock` `a` join `supply` `b`) join `supplier` `c`) join `product` `d` on(((`a`.`supply_id` = `b`.`supply_id`) and (`b`.`supplier_id` = `c`.`supplier_id`) and (`a`.`product_id` = `d`.`product_id`)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `receipt_id` (`receipt_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`receipt_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`stock_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `supply_id` (`supply_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `supply`
--
ALTER TABLE `supply`
  ADD PRIMARY KEY (`supply_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1600466;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `supply`
--
ALTER TABLE `supply`
  MODIFY `supply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`receipt_id`) REFERENCES `receipt` (`receipt_id`),
  ADD CONSTRAINT `purchase_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`);

--
-- Constraints for table `receipt`
--
ALTER TABLE `receipt`
  ADD CONSTRAINT `receipt_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `receipt_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `stock_ibfk_2` FOREIGN KEY (`supply_id`) REFERENCES `supply` (`supply_id`);

--
-- Constraints for table `supply`
--
ALTER TABLE `supply`
  ADD CONSTRAINT `supply_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`),
  ADD CONSTRAINT `supply_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
