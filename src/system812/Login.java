/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Santy Nana
 */
public class Login extends javax.swing.JFrame {

    /**
     * Creates new form Login
     */
    public Login() {
        initComponents();
    }
    
    

    
    public Connection getConnection() throws SQLException{
        Connection con;
        
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost/system812?useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            return con;
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
        return null;
        
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfusername = new javax.swing.JTextField();
        pfpassword = new javax.swing.JPasswordField();
        jButton_login = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1280, 720));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/system812/img/user-group-icon.png"))); // NOI18N

        jLabel2.setText("Username");

        jLabel3.setText("Password");

        tfusername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfusernameActionPerformed(evt);
            }
        });

        pfpassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pfpasswordActionPerformed(evt);
            }
        });
        pfpassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pfpasswordKeyPressed(evt);
            }
        });

        jButton_login.setText("Login");
        jButton_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_loginActionPerformed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfusername, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                            .addComponent(pfpassword)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jButton_login, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_cancel)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfusername, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addComponent(pfpassword))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton_login, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(81, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_loginActionPerformed
        // Login Button
       
        if(tfusername.getText().equalsIgnoreCase("")==false && pfpassword.getText().equalsIgnoreCase("")==false){
            
        try{
            
        String login_username = tfusername.getText();
        String login_password = pfpassword.getText();
        
      
        Employee foundEmployee = null;
        try {
            foundEmployee = Loginconsole.getLogin(login_username, login_password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (foundEmployee != null){
            tfusername.setText(foundEmployee.getlogin_username());
            pfpassword.setText(foundEmployee.getlogin_password());
            String getid = foundEmployee.getuser_id();
            //String getuser_type = foundEmployee.getuser_type();
            if("1".equals(getid)){
                new Adminform().setVisible(true);
                tfusername.setText("");
                pfpassword.setText("");
                this.setVisible(false);    
            }else{
                new Billingform(getid).setVisible(true);
                tfusername.setText("");
                pfpassword.setText("");
                this.setVisible(false);
            }
            
            
            
        }else{
              JOptionPane.showMessageDialog(this, "Invalid Credentials");
              tfusername.setText("");
              pfpassword.setText("");
              tfusername.requestFocus();   
        }
            
        }catch(Exception e){
            
        }
            
        }else{
            JOptionPane.showMessageDialog(null, "Fill UserName and Password");
        }
        
        
        
        
//        try{
//            
//            Connection con = getConnection();
//            Statement st = con.createStatement();
//            
//            switch(choose){   
//                
//                case ("Admin"):{
//                    ResultSet rs = st.executeQuery(" SELECT * FROM `user` where user_type = 'Admin' ");
//                    
//                    while(rs.next()){
//                        if((rs.getString("login_username").equals(user) && rs.getString("login_password").equals(password)) && rs.getString("user_type").equals(choose)){
//                            new Adminform().setVisible(true);
//                            this.setVisible(false);
//                            break;
//                        }
//                        
//                    }
//                        break;
//                }
//                
//                case ("Cashier"):{
//                    ResultSet rs = st.executeQuery(" SELECT * FROM `user` where user_type = 'Cashier' ");
//                    
//                    while(rs.next()){
//                        if((rs.getString("login_username").equals(user) && rs.getString("login_password").equals(password)) && rs.getString("user_type").equals(choose)){
//                            
//                            String getid = rs.getString("user_id");
//                            new Billingform().setVisible(true);
//                            //new Billingform(getid).setVisible(true);
//                            this.setVisible(false);
//                            
//                            break;
//                        }
//                        
//                    }
//                        break;
//                }
//                
//                default:{
//                    JOptionPane.showMessageDialog(null, "\t Invalid Username" + "\tPassword" + " User_Type11");
//                }
//                
//            }
//            
//            
//        }catch (SQLException e) {
//            
//        }
        

    }//GEN-LAST:event_jButton_loginActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // Buttom Cancel
        System.exit(0);
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void tfusernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfusernameActionPerformed
        // TODO add your handling code here:
        pfpassword.requestFocus();
    }//GEN-LAST:event_tfusernameActionPerformed

    private void pfpasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pfpasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pfpasswordActionPerformed

    private void pfpasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pfpasswordKeyPressed
        // TODO add your handling code here: PRESS LOGIN
       if(evt.getKeyCode()==KeyEvent.VK_ENTER){
           jButton_login.doClick();
       } 
       
        
    }//GEN-LAST:event_pfpasswordKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton jButton_login;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPasswordField pfpassword;
    private javax.swing.JTextField tfusername;
    // End of variables declaration//GEN-END:variables


}
