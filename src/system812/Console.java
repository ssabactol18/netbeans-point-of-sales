/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;

/**
 *
 * @author Santy Nana
 */


public class Console {
    
    public static Connection getConnection(){
        Connection con;
        
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost/system812?useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            return con;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        
    }
    
    
    //getEMployee
    
    
    public static Product getProduct(String product_id) throws ClassNotFoundException, SQLException{
        Connection con = getConnection();

       Statement st = con.createStatement();
       ResultSet rs = st.executeQuery("Select * From `product` where product_id= '" + product_id + "' ");
        
       if(rs.next()){
           Product product = new Product();
           product.setproduct_name(rs.getString("product_name"));
           product.setprice(rs.getDouble("price"));
           product.setquantity(rs.getInt("quantity"));
           return product;
           
       }else {
           return null;
       } 
       
    }
    
    public static String generateReceipt_id() throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(" Select `receipt_id` from `receipt` order by `receipt_id` desc limit 1 ");
        
        if(rs.next()){
            String receipt_id = rs.getString(1);//lastid
            receipt_id = (Integer.parseInt(receipt_id) + 1) + "";//id+1
            return receipt_id;

        }else{
           return "1"; 
        }
        
        
    }
    
    public static String generateSupply_receipt() throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(" Select `supply_id` from `supply` order by `supply_id` desc limit 1 ");
        
        if(rs.next()){
            String supply_id = rs.getString(1);//lastid
            supply_id = (Integer.parseInt(supply_id) + 1) + "";//id+1
            return supply_id;

        }else{
           return "1"; 
        }
        
        
    }
    
    
    
    public static String generateCustomer_id() throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(" Select `customer_id` from `customer` order by `customer_id` desc limit 1 ");
 
        if(rs.next()){
            String customer_id = rs.getString(1);//lastid
            customer_id = (Integer.parseInt(customer_id) + 1) + "";//date1
            return customer_id;
            
        }else{
            return "1";
        }
        
    }
    
    public static String generateSupply_id() throws ClassNotFoundException, SQLException{
        
        Connection con = getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(" Select `supply_id` from `supply` order by `supply_id` desc limit 1 ");
        
        if(rs.next()){
            String supply_id = rs.getString(1);
            supply_id = (Integer.parseInt(supply_id) + 1) +"";
            return supply_id;
        }else{
            return "1";
        }
    }
    
    public static String generateStock_id() throws ClassNotFoundException, SQLException{
        
        Connection con = getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(" Select `stock_id` from `stock` order by `stock_id` desc limit 1 ");
        
        if(rs.next()){
            String stock_id = rs.getString(1);
            stock_id = (Integer.parseInt(stock_id) + 1) +"";
            return stock_id;
        }else{
            return "1";
        }
    }
    


    public static boolean placeReceipt(Receipt receipt) throws ClassNotFoundException, SQLException {
        Connection con = getConnection();
        try{
            con.setAutoCommit(false);
            PreparedStatement ps;
            ps = con.prepareStatement("INSERT INTO `receipt` VALUES (?, ?, ?, ?, ?, ?); ");
            ps.setObject(1, receipt.getreceipt_id());
            ps.setObject(2, receipt.getuser_id());
            ps.setObject(3, receipt.getcustomer_id());
            ps.setObject(4, receipt.gettotal_qty());
            ps.setObject(5, receipt.gettotal_amount());
            ps.setObject(6, receipt.getorder_date());
            System.out.println(ps);

            
            boolean isAddedReceipt = ps.executeUpdate()>0;
            if(isAddedReceipt){
                boolean isUpdate = Console.updateProduct(receipt.getpurchaseDetailsList());
                con.setAutoCommit(false);
                con.commit();
                con.setAutoCommit(true);
                if(isUpdate){
                    boolean isPurchase = Console.addItemDetail(receipt.getpurchaseDetailsList());
                    if(isPurchase){
                        con.setAutoCommit(false);
                        con.commit();
                        return true;
                    }
                }
            }
            con.setAutoCommit(true);
            return true;
        }finally {
            con.setAutoCommit(true);
            
          
        }
        
        
    }
    
    
    public static boolean addItemDetail(ArrayList<Purchase> purchaseDetailsList) throws ClassNotFoundException, SQLException{
        for (Purchase itemDetails : purchaseDetailsList){
            if(!addItemDetail(itemDetails)){
                return false;
            }
        }
        return true;
    }
      public static boolean addItemDetail(Purchase itemDetails) throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        try{
            con.setAutoCommit(false);
         PreparedStatement ps = con.prepareStatement("INSERT INTO `purchase`(`receipt_id`, `product_id`, `purchase_quantity`, `purchase_price`) VALUES (?, ?, ?, ?);" );
            ps.setObject(1, itemDetails.getreceipt_id());
            ps.setObject(2, itemDetails.getproduct_id());
            ps.setObject(3, itemDetails.getpurchase_quantity());
            ps.setObject(4, itemDetails.getpurchase_price());
             System.out.println(ps);
            con.setAutoCommit(true);
           return ps.executeUpdate()>0;
        
        }finally{
            con.setAutoCommit(true); 
            return true;
        }
    
        
    }
    
       public static boolean updateProduct(ArrayList<Purchase> purchaseDetailsList) throws ClassNotFoundException, SQLException {
       for (Purchase itemDetails : purchaseDetailsList){
            if(!updateProduct(itemDetails)){
                return false;
            }
        }
        return true;
    }

    public static boolean updateProduct(Purchase itemDetails) throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        try{
        con.setAutoCommit(false);
        Statement st = con.createStatement();
        String sql = ("UPDATE `product` SET `quantity` = quantity - '"+itemDetails.getpurchase_quantity()+"' WHERE `product`.`product_id` = '"+itemDetails.getproduct_id()+"' ; ");
        System.out.println(sql);
        return st.executeUpdate(sql)>0;    
        
        
        }finally{
            con.setAutoCommit(true); 
            return true;
        }
     
        
    }
    
    //================================================================================================================
    
    public static boolean placeSupply(Supply supply) throws ClassNotFoundException, SQLException {
        Connection con = getConnection();
        try{
            con.setAutoCommit(false);
            PreparedStatement ps;
            ps = con.prepareStatement("INSERT INTO `supply` VALUES (?, ?, ?, ?, ?); ");
            ps.setObject(1, supply.getsupply_id());
            ps.setObject(2, supply.getsupplier_id());
            ps.setObject(3, supply.getsupply_receipt());
            ps.setObject(4, supply.getuser_id());
            ps.setObject(5, supply.getdate_receive());
            System.out.println(ps);

            
            boolean isAddedSupply = ps.executeUpdate()>0;
            if(isAddedSupply){
                boolean isUpdate = Console.updateProduct1(supply.getstockDetailsList());
                con.setAutoCommit(false);
                con.commit();
                con.setAutoCommit(true);
                if(isUpdate){
                    boolean isPurchase = Console.addItemDetail1(supply.getstockDetailsList());
                    if(isPurchase){
                        con.setAutoCommit(false);
                        con.commit();
                        return true;
                    }
                }
            }
            con.setAutoCommit(true);
            return true;
        }finally {
            con.setAutoCommit(true);
            
          
        }
        
        
    }
    
    
        
    public static boolean addItemDetail1(ArrayList<Stock> stockDetailsList) throws ClassNotFoundException, SQLException{
        for (Stock itemDetails : stockDetailsList){
            if(!addItemDetail1(itemDetails)){
                return false;
            }
        }
        return true;
    }
      public static boolean addItemDetail1(Stock itemDetails) throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        try{
            con.setAutoCommit(false);
         PreparedStatement ps = con.prepareStatement("INSERT INTO `stock`( `product_id`, `supply_id`, `quantity`, `purchase_price`) VALUES ( ?, ?, ?, ?);" );
            //ps.setObject(1, itemDetails.getstock_id());
            ps.setObject(1, itemDetails.getproduct_id());
            ps.setObject(2, itemDetails.getsupply_id());
            ps.setObject(3, itemDetails.getquantity());
            ps.setObject(4, itemDetails.getpurchase_price());
             System.out.println(ps);
            con.setAutoCommit(true);
           return ps.executeUpdate()>0;
        
        }finally{
            con.setAutoCommit(true); 
            //return true;
        }
    
        
    }
    
       public static boolean updateProduct1(ArrayList<Stock> stockDetailsList) throws ClassNotFoundException, SQLException {
       for (Stock itemDetails : stockDetailsList){
            if(!updateProduct1(itemDetails)){
                return false;
            }
        }
        return true;
    }

    public static boolean updateProduct1(Stock itemDetails) throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        try{
        con.setAutoCommit(false);
        Statement st = con.createStatement();
        String sql = ("UPDATE `product` SET `quantity` = quantity + '"+itemDetails.getquantity()+"' WHERE `product`.`product_id` = '"+itemDetails.getproduct_id()+"' ; ");
        System.out.println(sql);
        return st.executeUpdate(sql)>0;    
        
        
        }finally{
            con.setAutoCommit(true); 
            return true;
        }
     
        
    }


    
}
