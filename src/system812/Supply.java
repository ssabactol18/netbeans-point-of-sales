/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

import java.util.ArrayList;

/**
 *
 * @author Santy Nana
 */
public class Supply {
                      
    private String supply_id;
    private String supplier_id;
    private String supply_receipt;
    private String user_id;
    private String date_receive;
    
    ArrayList<Stock> stockDetailsList;
    
    public Supply(){
    
    }

    public Supply(String supply_id, String supplier_id, String supply_receipt, String user_id, String date_receive, ArrayList<Stock>stockDetailsList){
        this.supply_id = supply_id;
        this.supplier_id = supplier_id;
        this.supply_receipt = supply_receipt;
        this.user_id = user_id;
        this.date_receive = date_receive;
        this.stockDetailsList = stockDetailsList;
    }
    
    public void setsupply_id(String supply_id){
        this.supply_id = supply_id;
    }
    public String getsupply_id(){
        return supply_id;
    }
    
    public void setsupplier_id(String supplier_id){
        this.supplier_id = supplier_id;
    }
    public String getsupplier_id(){
        return supplier_id;
    }
    public void setsupply_receipt(String supply_receipt){
        this.supply_receipt = supply_receipt;
    }
    public String getsupply_receipt(){
        return supply_receipt;
    }
    public void setuser_id(String user_id){
        this.user_id = user_id;
    }
    public String getuser_id(){
        return user_id;
    }
    public void setdate_receive(String date_receive){
        this.date_receive = date_receive;
    }
    public String getdate_receive(){
        return date_receive;
    }
    
    public void setstockDetailsList(ArrayList<Stock> stockDetailsList){
        this.stockDetailsList = stockDetailsList;
    }
    public ArrayList<Stock> getstockDetailsList(){
        return stockDetailsList;
    }
                            
    
}
