/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

/**
 *
 * @author Santy Nana
 */
public class Employee {
    
    private String user_id;
    private String user_type;
    private String login_username;
    private String login_password;
    private String user_fname;
    private String user_mname;
    private String user_lname;
    private String contact_no;
    private String birthday;
    private String address;
    private String email;
    
    public Employee(){
        
    }
    
    public Employee(String user_id, String user_type, String login_username, String login_password, String user_fname, String user_mname, String user_lname,String contact_no, String birthday, String address, String email){
        this.user_id = user_id;
        this.user_type = user_type;
        this.login_username = login_username;
        this.login_password = login_password;
        this.user_fname = user_fname;
        this.user_mname = user_mname;
        this.user_lname = user_lname;
        this.contact_no = contact_no;
        this.birthday = birthday;
        this.address = address;
        this.email = email;
    }
    
    public void setuser_id(String user_id){
        this.user_id = user_id;
    }
    public void setuser_type(){
        this.user_type = user_type;
    }
    public void setlogin_username(String login_username){
        this.login_username = login_username;
    }
    public void setlogin_password(String login_password){
        this.login_password = login_password;
    }
    public void setuser_fname(){
        this.user_fname = user_fname;
    }
    public void setuser_mname(){
        this.user_mname = user_mname;
    }
    public void setuser_lname(){
        this.user_lname = user_lname;
    }
    public void setcontact_no(){
        this.contact_no = contact_no;
    }
    public void setbirthday(){
        this.birthday = birthday;
    }
    public void setaddress(){
        this.address = address;
    }
    public void setemail(){
        this.email = email;
    }
    
    public String getuser_id(){
        return user_id;
    }
    public String getuser_type(){
        return user_type;
    }
    public String getlogin_username(){
        return login_username;
    }
    public String getlogin_password(){
        return login_password;
    }
    public String getuser_fname(){
        return user_fname;
    }
    public String getuser_mname(){
        return user_mname;
    }
    public String getuser_lname(){
        return user_lname;
    }
    public String getcontact_no(){
        return contact_no;
    }
    public String getbirthday(){
        return birthday;
    }
    public String getaddress(){
        return address;
    }
    public String getemail(){
        return email;
    }  
    
}
