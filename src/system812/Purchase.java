/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

/**
 *
 * @author Santy Nana
 */
public class Purchase {
    //private String product_code;
    private String receipt_id;
    private String product_id;
    private String purchase_id;
    private String product_name;
    private int purchase_quantity;
    private double purchase_price;
            
    public Purchase(){
        
    }
    //purchase_id, receipt_id, product_id, purchase_quantity, price
    public Purchase(String receipt_id, String product_id, int purchase_quantity, double purchase_price){
        this.receipt_id = receipt_id;
        this.product_id = product_id;
        this.purchase_quantity = purchase_quantity;
        this.purchase_price = purchase_price;
        //this.product_code = product_code
        //this.purchase_id = purchase_id;
    }
    
//    public Purchase(String purchase_id, String receipt_id, String product_id, String product_name, int purchase_quantity, double price){
//        this.receipt_id = receipt_id;
//        this.product_id = product_id;
//        this.product_name = product_name;
//        this.purchase_quantity = purchase_quantity;
//        this.price = price;
//        this.purchase_id = purchase_id;
//        //this.product_code = product_code;
//    }
//    public void setproduct_code(){
//        this.product_code = product_code;
//    }
//    public String getproduct_code(){
//        return product_code;
//    }
    
    public void setpurchase_id(String purchase_id){
        this.purchase_id = purchase_id;
    }
    public String getpurchase_id(){
        return purchase_id;
    }
    
    public void setreceipt_id(String receipt_id){
        this.receipt_id = receipt_id;
    }
    public void setproduct_id(String product_id){
        this.product_id = product_id;
    }
    public void setproduct_name(String product_name){
        this.product_name = product_name;
    }
    public void setpurchase_quantity(int purchase_quantity){
        this.purchase_quantity = purchase_quantity;
    }
    public void setprice(double purchase_price){
        this.purchase_price = purchase_price;
    }

    public String getreceipt_id(){
        return receipt_id;
    }
    public String getproduct_id(){
        return product_id;
    }
    public String getproduct_name(){
        return product_name;
    }
    public int getpurchase_quantity(){
        return purchase_quantity;
    }
    public Double getpurchase_price(){
        return purchase_price;
    } 
    
}
