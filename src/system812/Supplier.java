/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

/**
 *
 * @author Santy Nana
 */
public class Supplier {
    
    private String supplier_id;
    private String supplier_name;
    private String supplier_address;
    private String supplier_contact;
    private String supplier_email;
    
    public Supplier(){
        
    }
    
    public Supplier(String supplier_id, String supplier_name, String supplier_address, String supplier_contact, String supplier_email){
        this.supplier_id = supplier_id;
        this.supplier_name = supplier_name;
        this.supplier_address = supplier_address;
        this.supplier_contact = supplier_contact;
        this.supplier_email = supplier_email;
    }
    
    public void setsupplier_id(String supplier_id){
        this.supplier_id = supplier_id;
    }
    public String getsupplier_id(){
        return supplier_id;
    }
    
    public void setsupplier_name(String supplier_name){
        this.supplier_name = supplier_name;
    }
    public String getsupplier_name(){
        return supplier_name;
    }
    public void setsupplier_address(String supplier_address){
        this.supplier_address = supplier_address;
    }
    public String getsupplier_address(){
        return supplier_address;
    }
    public void setsupplier_contact(String supplier_contact){
        this.supplier_contact = supplier_contact;
    }
    public String getsupplier_contact(){
        return supplier_contact;
    }
    public void setsupplier_email(String supplier_email){
        this.supplier_email = supplier_email;
    }
    public String getsupplier_email(){
        return supplier_email;
    }
    
    
    
    
}
