/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

import java.util.ArrayList;

/**
 *
 * @author Santy Nana
 */
public class Receipt {
    
    private String receipt_id;
    private String user_id;
    private String customer_id;
    
    private String total_qty;
    private String total_amount;
    private String order_date;
    
    private ArrayList<Purchase> purchaseDetailsList;
    
    public Receipt(){
        
    }
    
    public Receipt(String receipt_id,String user_id,String customer_id,String total_qty,String total_amount,String order_date, ArrayList<Purchase> purchaseDetailsList){
      this.receipt_id = receipt_id;
      this.user_id = user_id;
      this.customer_id = customer_id;
      this.total_amount = total_amount;
      this.total_qty = total_qty;
      this.order_date = order_date;
      this.purchaseDetailsList = purchaseDetailsList;
    }


   
    public void setuser_id(String user_id){
        this.user_id = user_id;
    }
    public String getuser_id(){
        return user_id;
    }
    
    
    public void setreceipt_id(String receipt_id){
        this.receipt_id = receipt_id;
    }
    public void setcustomer_id(String customer_id){
        this.customer_id = customer_id;
    }
    public void settotal_qty(String total_qty){
        this.total_qty = total_qty;
    }
    public void settotal_amount(String total_amount){
        this.total_amount = total_amount;
    }
    public void setorder_date(String order_date){
        this.order_date = order_date;
    }
    public void setpurchaseDetailsList(ArrayList<Purchase> purchaseDetailsList){
        this.purchaseDetailsList = purchaseDetailsList;
    } 
    
    public String getreceipt_id(){
        return receipt_id;
    }
    public String getcustomer_id(){
        return customer_id;
    }
    public String gettotal_qty(){
        return total_qty;
    }
    public String gettotal_amount(){
        return total_amount;
    }
    public String getorder_date(){
        return order_date;
    }
    public ArrayList<Purchase> getpurchaseDetailsList(){
        return purchaseDetailsList;
    }
    
}
