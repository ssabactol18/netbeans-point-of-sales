/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Santy Nana
 */
public class DBConnection {
    
    private Connection con;
    private static DBConnection dBConnection;
    
    private DBConnection() throws ClassNotFoundException, SQLException{
        con = DriverManager.getConnection("jdbc:mysql://localhost/system812?useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
    }
    public static DBConnection getInstance() throws ClassNotFoundException, SQLException{
        if(dBConnection==null){
            dBConnection=new DBConnection();
        }
        return dBConnection;
    }
    public Connection getConnection(){
        return con;
    }
    
}
