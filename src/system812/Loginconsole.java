/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Santy Nana
 */
public class Loginconsole {
    
        public static Connection getConnection(){
        Connection con;
        
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost/system812?useUnicode=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            return con;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
        
    }
    
        public static Employee getLogin(String login_username, String login_password) throws ClassNotFoundException, SQLException{
        Connection con = getConnection();
        Statement st = con.createStatement();
        ResultSet rs;
            rs = st.executeQuery("SELECT * FROM user WHERE login_username= '" + login_username + "' AND login_password= '" + login_password + "' ");
        if (rs.next()) {
            Employee emp = new Employee();
            emp.setuser_id(rs.getString("user_id"));
            emp.setlogin_username(rs.getString("login_username"));
            emp.setlogin_password(rs.getString("login_password"));
            return emp;
        }else {
            return null;
        }
          
        }
        
    
}
