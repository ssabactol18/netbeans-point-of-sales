/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

/**
 *
 * @author Santy Nana
 */
public class Product {
    
    private int product_id;
    private String product_name;
    private double price;
    private int quantity;
    private String category;
    private String description;
    

    public Product(){
    }
    
    
    public Product(int product_id, String product_name, double price, int quantity, String category, String description)
    {
        
        this.product_id = product_id;
        this.product_name = product_name;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
        this.description = description;
        
    }

    public void setproduct_id(int product_id){
        this.product_id = product_id;
    }
    public int getproduct_id(){
        return product_id;
    }
    public void setproduct_name(String product_name){
        this.product_name = product_name;
    }
    public String getproduct_name(){
        return product_name;
    }
    public void setprice(double price){
        this.price = price;
    }
    public double getprice(){
        return price;
    }
    public void setquantity(int quantity){
        this.quantity = quantity;
    }
    public int getquantity(){
        return quantity;
    }
    public void category(){
        this.category = category;
    }
    public String getcategory(){
        return category;
    }
    public void description(){
        this.description = description;
    }
    public String getdescription(){
        return description;
    }

 
    
}
