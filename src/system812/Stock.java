/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system812;

/**
 *
 * @author Santy Nana
 */
public class Stock {
    
    private String stock_id;
    private String product_id;
    private String supply_id;
    private int quantity;
    private double purchase_price;
    
    public Stock(){
        
    }
    
    public Stock(String stock_id, String product_id, String supply_id, int quantity, double purchase_price){
        this.stock_id = stock_id;
        this.product_id = product_id;
        this.supply_id = supply_id;
        this.quantity = quantity;
        this.purchase_price = purchase_price;
        
    }
    
    public void setstock_id(String stock_id){
        this.stock_id = stock_id;
    }
    public String getstock_id(){
        return stock_id;
    }
    public void setproduct_id(String product_id){
        this.product_id = product_id;
    }
    public String getproduct_id(){
        return product_id;
    }
    public void setsupply_id(String supply_id){
        this.supply_id = supply_id;
    }
    public String getsupply_id(){
        return supply_id;
    }
    public void setquantity(int quantity){
        this.quantity = quantity;
    }
    public int getquantity(){
        return quantity;
    }
    public void setpurchase_price(double purchase_price){
        this.purchase_price = purchase_price;
    }
    public double getpurchase_price(){
        return purchase_price;
    }
    
   
    
    
}
